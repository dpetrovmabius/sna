{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import Data.Text (Text)
import Haxl.Core (Env(..))
import Data.Monoid ((<>))
import Control.Applicative ((<*>))
import Data.Time (UTCTime, getCurrentTime)
import FBHaxl
import GrabPage
import Utils
import NSLogger
import Facebook (Id(..), FacebookException(..))

import System.Log.FastLogger
import Options.Applicative
import Database.SQLite.Simple
import Control.Exception


data FBPage = FBPage
    { fbPageId :: Text
    , fbPageCreatedTime :: UTCTime
    , fbPageUpdatedTime :: UTCTime
    } deriving (Eq,Show,Ord)


instance FromRow FBPage where
    fromRow = FBPage <$> field <*> field <*> field

data CrawlerArgs = CrawlerArgs
    { caDBPath :: FilePath
    , caLogPath :: FilePath
    , caCfgPath :: FilePath
    }

crawlerArgs :: Parser CrawlerArgs
crawlerArgs =
    CrawlerArgs <$>
    strOption
        (long "dbpath" <> metavar "FILE" <> help "Path to SQLite db file") <*>
    strOption (long "logpath" <> metavar "FILE" <> help "Path to log folder")<*>
    strOption (long "cfgpath" <> metavar "FILE" <> help "Path to cfg file")


grabPage :: LoggerSet -> Id -> UTCTime -> Env () -> IO ()
grabPage = logAllInfoAboutPage


opts =
    info
        (helper <*> crawlerArgs)
        (fullDesc <> progDesc "Grab info from registered in db pages" <>
         header "sna-hcrawler - social network analysis crawler")

main :: IO ()
main = do
    ca <- execParser opts
    utctime <- getCurrentTime
    loggerset <- newFileLoggerSet defaultBufSize (caLogPath ca <> "/crawler.log")
    env' <- haxlEnv (caCfgPath ca)
    conn <- open (caDBPath ca)
    fbpages <- query_ conn "SELECT * FROM fbPage" :: IO [FBPage]
    mapM_
        (\fbpage ->
              executeNamed
                  conn
                  "UPDATE fbPage SET updated_time = :updated_time WHERE pid = :pid"
                  [":updated_time" := utctime, ":pid" := fbPageId fbpage] >>
              catch
                  (grabPage
                       loggerset
                       (Id (fbPageId fbpage))
                       (fbPageUpdatedTime fbpage)
                       env')
                  (\e ->
                        case e of
                            FacebookException _ _ -> do
                                pushLogStrLn
                                    loggerset
                                    (logNS utctime ERROR (show e))
                                execute
                                    conn
                                    "DELETE FROM fbPage WHERE pid = ?"
                                    (Only (fbPageId fbpage))
                            _ ->
                                pushLogStrLn
                                    loggerset
                                    (logNS utctime ERROR (show e))))
        fbpages
    flushLogStr loggerset
