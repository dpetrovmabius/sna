{-# LANGUAGE OverloadedStrings #-}
module Utils
    ( utc2bytestring
    , shiftTo2weeksBack
    , FBAction(..)
    , getAppToken
    , getCredentials
    , haxlEnv
    ) where

import FBHaxl (initGlobalState)
import Haxl.Core (Env(..), initEnv, stateEmpty, stateSet)
import Data.Text(Text)
import Data.Time
       (UTCTime, formatTime, defaultTimeLocale, addUTCTime)
import Data.Configurator (Worth(..))
import qualified Data.Configurator as Conf
import qualified Data.ByteString.Char8 as BSC
import Network.HTTP.Conduit (newManager)
import Network.HTTP.Client.OpenSSL
       (opensslManagerSettings)
import qualified OpenSSL.Session as SSL
import Facebook
       (Id(..), Credentials(..), AppAccessToken, runFacebookT,
        getAppAccessToken)
import Control.Monad.Trans.Resource (runResourceT)
import Data.Aeson (ToJSON(..), object, (.=), Value)


-- | Convert utc time to unix timestamp and convert to bytestirng
utc2bytestring :: UTCTime -> BSC.ByteString
utc2bytestring = BSC.pack . formatTime defaultTimeLocale "%s"

shiftTo2weeksBack :: UTCTime -> UTCTime
shiftTo2weeksBack = addUTCTime (-14 * 24 * 60 * 60)

data FBAction = FBAction
    { _who :: Id
    , _where :: Id
    , _what :: Id
    , _action :: Text
    , _entity :: Value
    }

instance ToJSON FBAction where
    toJSON (FBAction _who _where _what _action _entity) =
        object
            [ "who" .= idCode _who
            , "where" .= idCode _where
            , "action" .= _action
            , "what" .= idCode _what
            , "entity" .= _entity]


getAppToken :: Credentials -> IO AppAccessToken
getAppToken creds = do
    manager <- newManager (opensslManagerSettings SSL.context)
    runResourceT $ runFacebookT creds manager getAppAccessToken

getCredentials :: FilePath -> IO Credentials
getCredentials cfgpath = do
    config <- Conf.load [Required cfgpath]
    appName' <- Conf.require config "fb.app_name"
    appId' <- Conf.require config "fb.app_id"
    appSecret' <- Conf.require config "fb.app_secret"
    return
        Credentials
        { appName = appName'
        , appId = appId'
        , appSecret = appSecret'
        }

haxlEnv :: FilePath -> IO (Env ())
haxlEnv cfgpath = do
    creds <- getCredentials cfgpath
    tok <- getAppToken creds
    facebookState <- initGlobalState 2 creds tok
    initEnv (stateSet facebookState stateEmpty) ()

