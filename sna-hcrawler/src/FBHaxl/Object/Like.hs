{-# LANGUAGE OverloadedStrings
           , FlexibleContexts #-}
module FBHaxl.Object.Like (getLikes) where

import Data.Monoid ((<>))
import Control.Monad.Trans.Resource (MonadResource)
import Control.Monad.Trans.Control (MonadBaseControl)
import Facebook
       (Id(..), Argument, AccessToken, FacebookT, Pager,
        getObject, User)

-- | Get the list of likes of the given object
getLikes
    :: (MonadResource m, MonadBaseControl IO m)
    => Id
    -> [Argument]
    -> AccessToken anyKind
    -> FacebookT anyAuth m (Pager User)
getLikes id_ query tok =
    getObject ("/" <> idCode id_ <> "/likes") query (Just tok)
