{-# LANGUAGE OverloadedStrings #-}
module FBHaxl.Object.User where

import Facebook (User(..))
import Data.Aeson (object, (.=), ToJSON(..))


instance ToJSON User where
    toJSON u = object ["id" .= userId u]
