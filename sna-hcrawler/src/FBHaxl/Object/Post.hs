{-# LANGUAGE OverloadedStrings
           , FlexibleContexts #-}
module FBHaxl.Object.Post
       (Post(..), getPost, getPostComments, getSharedPosts) where

import Data.Typeable
import Data.Monoid ((<>))
import Control.Monad (mzero)
import Control.Applicative ((<$>), (<*>))
import Data.Text (Text)
import Data.Time (UTCTime)
import Data.Aeson ((.:), (.:?), object, (.=))
import qualified Data.Aeson as A
import Control.Monad.Trans.Resource (MonadResource)
import Control.Monad.Trans.Control (MonadBaseControl)
import Facebook
       (Id(..), Argument, AccessToken, FacebookT, Pager, User(..),
        getObject)


import FBHaxl.Object.Comment
import FBHaxl.Object.Like
import FBHaxl.Object.Counter

import Debug.Trace
import Data.Text

-- | A Facebook post (see
-- <https://developers.facebook.com/docs/reference/api/page>).
--
-- /NOTE:/ Does not yet support all fields. Please file an issue if
-- you need any other fields.
data Post = Post
    { postId :: Id
    , shares :: Maybe Counter
    , postCreatedTime :: Maybe UTCTime
    , postType :: Maybe Text
    , postMessage :: Maybe Text
    , postFrom :: Maybe User
    } deriving (Eq,Ord,Show,Read,Typeable)


instance A.FromJSON Post where
    parseJSON (A.Object v) =
        Post <$> v .: "id" <*> v .:? "shares" <*> v .:? "created_time" <*>
        v .:? "type" <*>
        v .:? "message" <*>
        v .:? "from" 
    parseJSON _ = mzero


instance A.ToJSON Post where
    toJSON (Post pid sh pct pt pm pf ) =
        object
            [ "id" .= pid
            , "shares" .= sh
            , "created_time" .= pct
            , "type" .= pt
            , "message" .= pm
            , "from" .= pf
            ]


-- | Get a post using its ID.
getPost
    :: (MonadResource m, MonadBaseControl IO m)
    => Id -> [Argument] -> AccessToken anyKind -> FacebookT anyAuth m Post
getPost id_ query tok = getObject ("/" <> idCode id_) query (Just tok)


-- | Get the list of likes of the given post
getPostLikes
    :: (MonadResource m, MonadBaseControl IO m)
    => Id
    -> [Argument]
    -> AccessToken anyKind
    -> FacebookT anyAuth m (Pager User)
getPostLikes = getLikes


-- | Get the list of comments of the given post
getPostComments
    :: (MonadResource m, MonadBaseControl IO m)
    => Id
    -> [Argument]
    -> AccessToken anyKind
    -> FacebookT anyAuth m (Pager Comment)
getPostComments = getComments


-- | Get the list of sharedposts of the given post
getSharedPosts
    :: (MonadResource m, MonadBaseControl IO m)
    => Id
    -> [Argument]
    -> AccessToken anyKind
    -> FacebookT anyAuth m (Pager Post)
getSharedPosts id_ query tok =
    getObject ("/" <> idCode id_ <> "/sharedposts") query (Just tok)
