{-# LANGUAGE OverloadedStrings
           , FlexibleContexts#-}
module FBHaxl.Object.Page (getPage, getPageLikes, getPagePosts)
       where

import Data.Monoid ((<>))
import Control.Monad.Trans.Resource (MonadResource)
import Control.Monad.Trans.Control (MonadBaseControl)
import Data.Aeson (ToJSON(..), object, (.=))
import Facebook
       (Page(..), Id(..), Argument, AccessToken, FacebookT, Pager, User(..),
        getObject)

import FBHaxl.Object.Post


instance ToJSON Page where
    toJSON p =
        object
            [ "id" .= pageId p
            , "name" .= pageName p
            , "link" .= pageLikes p
            , "category" .= pageCategory p
            , "is_published" .= pageIsPublished p
            , "can_post" .= pageCanPost p
            , "likes" .= pageLikes p
            , "phone" .= pagePhone p
            , "checkins" .= pageCheckins p
            , "picture" .= pagePicture p
            , "website" .= pageWebsite p
            , "talking_about_count" .= pageTalkingAboutCount p]


-- | Get a page using its ID
getPage
    :: (MonadResource m, MonadBaseControl IO m)
    => Id -> [Argument] -> AccessToken anyKind -> FacebookT anyAuth m Page
getPage id_ query tok = getObject ("/" <> idCode id_) query (Just tok)

-- | Get the list of posts of the given page
getPagePosts
    :: (MonadResource m, MonadBaseControl IO m)
    => Id
    -> [Argument]
    -> AccessToken anyKind
    -> FacebookT anyAuth m (Pager Post)
getPagePosts id_ query tok =
    getObject ("/" <> idCode id_ <> "/posts") query (Just tok)


-- | Get the list of users who like this page
getPageLikes
    :: (MonadResource m, MonadBaseControl IO m)
    => Id
    -> [Argument]
    -> AccessToken anyKind
    -> FacebookT anyAuth m (Pager User)
getPageLikes id_ query tok =
    getObject ("/" <> idCode id_ <> "/likes") query (Just tok)
