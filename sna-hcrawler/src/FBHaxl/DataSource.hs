{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module FBHaxl.DataSource where


import Data.Hashable
import Data.Typeable
import Data.Monoid ((<>))
import Network.HTTP.Conduit (newManager, Manager)
import Network.HTTP.Client.OpenSSL
       (opensslManagerSettings)
import qualified OpenSSL.Session as SSL
import Control.Concurrent.QSem
       (newQSem, waitQSem, signalQSem, QSem)
import Control.Concurrent.Async (wait, async, Async)
import Facebook
       (Id(..), Credentials(..), runFacebookT, FacebookT, getObject, Auth,
        AppAccessToken, Argument, Page(..), User(..), fetchAllNextPages)
import Control.Monad.Trans.Resource (runResourceT, ResourceT)
import Control.Exception
import Data.Conduit (($$))
import Data.Conduit.List (consume)

import Haxl.Core

import FBHaxl.Object.Page
import FBHaxl.Object.Post
import FBHaxl.Object.Like
import FBHaxl.Object.Comment


data FacebookReq a where
        GetPage :: Id -> [Argument] -> FacebookReq Page
        GetLikes :: Id -> [Argument] -> FacebookReq [User]
        GetPagePosts :: Id -> [Argument] -> FacebookReq [Post]
        GetPostComments :: Id -> [Argument] -> FacebookReq [Comment]
        GetPostReposts :: Id -> [Argument] -> FacebookReq [Post]
    deriving Typeable


deriving instance Eq (FacebookReq a)
deriving instance Show (FacebookReq a)

instance Show1 FacebookReq where show1 = show


instance Hashable (FacebookReq a) where
    hashWithSalt s (GetPage (Id id) args) = hashWithSalt s (0 :: Int, id, args)
    hashWithSalt s (GetLikes (Id id) args) =
        hashWithSalt s (1 :: Int, id, args)
    hashWithSalt s (GetPagePosts (Id id) args) =
        hashWithSalt s (2 :: Int, id, args)
    hashWithSalt s (GetPostComments (Id id) args) =
        hashWithSalt s (3 :: Int, id, args)
    hashWithSalt s (GetPostReposts (Id id) args) =
        hashWithSalt s (2 :: Int, id, args)

instance StateKey FacebookReq where
    data State FacebookReq = FacebookState{credentials :: Credentials,
                                       accessToken :: AppAccessToken, manager :: Manager,
                                       numThreads :: Int}

instance DataSourceName FacebookReq where
    dataSourceName _ = "Facebook"


instance DataSource u FacebookReq where
    fetch = facebookFetch


initGlobalState :: Int
                -> Credentials
                -> AppAccessToken
                -> IO (State FacebookReq)
initGlobalState threads creds tok = do
    manager <- newManager (opensslManagerSettings SSL.context)
    return
        FacebookState
        { credentials = creds
        , accessToken = tok
        , manager = manager
        , numThreads = threads
        }


facebookFetch :: State FacebookReq
              -> Flags
              -> u
              -> [BlockedFetch FacebookReq]
              -> PerformFetch
facebookFetch FacebookState{..} _flags _user bfs =
    SyncFetch $ mapM_ (fetchSync credentials accessToken manager) bfs


fetchSync
    :: Credentials
    -> AppAccessToken
    -> Manager
    -> BlockedFetch FacebookReq
    -> IO ()
fetchSync creds tok manager (BlockedFetch req rvar) =
    do e <-
           Control.Exception.try $
           runResourceT $ runFacebookT creds manager $ fetchReq tok req
       case e of
           Left ex -> putFailure rvar (ex :: SomeException)
           Right a -> putSuccess rvar a

fetchReq :: AppAccessToken
         -> FacebookReq a
         -> FacebookT Auth (ResourceT IO) a
fetchReq tok (GetPage id_ args) = getPage id_ args tok
fetchReq tok (GetLikes id_ args) = do
    us <- getLikes id_ args tok
    source <- fetchAllNextPages us
    source $$ consume
fetchReq tok (GetPagePosts id_ args) = do
    ps <- getPagePosts id_ args tok
    source <- fetchAllNextPages ps
    source $$ consume
fetchReq tok (GetPostComments id_ args) = do
    cs <- getPostComments id_ args tok
    source <- fetchAllNextPages cs
    source $$ consume
fetchReq tok (GetPostReposts id_ args) = do
    sps <- getSharedPosts id_ args tok
    source <- fetchAllNextPages sps
    source $$ consume
