
module FBHaxl
       (getPage, Page(..), initGlobalState, getLikes, getPagePosts,
        Post(..), getPostComments, getPostReposts, Comment(..))
       where

import Facebook (Id(..), Argument, Page(..), User(..))

import Haxl.Core

import FBHaxl.DataSource
import FBHaxl.Object.Post (Post(..))
import FBHaxl.Object.Comment (Comment(..))

getPage :: Id -> [Argument] -> GenHaxl u Page
getPage id_ args = dataFetch (GetPage id_ args)


getLikes :: Id -> [Argument] -> GenHaxl u [User]
getLikes id_ args = dataFetch (GetLikes id_ args)


getPagePosts :: Id -> [Argument] -> GenHaxl u [Post]
getPagePosts id_ args = dataFetch (GetPagePosts id_ args)


getPostComments :: Id -> [Argument] -> GenHaxl u [Comment]
getPostComments id_ args = dataFetch (GetPostComments id_ args)


getPostReposts :: Id -> [Argument] -> GenHaxl u [Post]
getPostReposts id_ args = dataFetch (GetPostReposts id_ args)
