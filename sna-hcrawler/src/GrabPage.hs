{-# LANGUAGE OverloadedStrings #-}

module GrabPage
    ( logAllInfoAboutPage
    ) where

import Data.Monoid ((<>))
import Data.Maybe (fromMaybe, fromJust)
import Data.Time (UTCTime)
import Facebook (Id(..), User(..))
import Data.Aeson (toJSON)
import Haxl.Core (Env(..), runHaxl)
import System.Log.FastLogger (LoggerSet, pushLogStrLn)

import NSLogger
import FBHaxl
import Utils


collectAllInfoAboutPage :: Id -> UTCTime -> Env u -> IO (Page, [User], [Post])
collectAllInfoAboutPage pid utctime env' =
    runHaxl env' $
    do page <-
           getPage
               pid
               [ ( "fields"
                 , "id,likes,name,talking_about_count,phone,checkins," <>
                   "category,can_post,website,is_published")]
       (likers,posts) <-
           (,) <$> getLikes (pageId page) [("fields", "id")] <*>
           getPagePosts
               (pageId page)
               [ ("fields", "id,shares,from,type,created_time,message")
               , ("since", utc2bytestring utctime)]
       return (page, likers, posts)


collectAllPostInfo :: [Post]
                   -> UTCTime
                   -> Env u
                   -> IO [(Post, ([User], [Comment], [Post]))]
collectAllPostInfo posts _utctime env' =
    runHaxl env' $
    mapM
        (\p ->
              (,,) <$> getLikes (postId p) [("fields", "id")] <*>
              getPostComments
                  (postId p)
                  [("fields", "id,from,created_time,like_count,message")] <*>
              getPostReposts
                  (postId p)
                  [("fields", "id,from,shares,created_time,type")] >>=
              \u ->
                   return (p, u))
        posts


logAllPostsInfo
    :: LoggerSet -> [Post] -> UTCTime -> Env u -> IO ()
logAllPostsInfo _ [] _ _ = return ()
logAllPostsInfo loggerset posts utctime env' = do
    postsInfo <- collectAllPostInfo posts utctime env'
    reposts <-
        mapM
            (\(p,(us,cs,rs)) ->
                  do mapM_
                         (\u ->
                               pushLogStrLn
                                   loggerset
                                   (logNS
                                        utctime
                                        INFO
                                        FBAction
                                        { _who = userId u
                                        , _where = postId p
                                        , _action = "like"
                                        , _what = postId p
                                        , _entity = toJSON u
                                        }))
                         us
                     mapM_
                         (\c ->
                               pushLogStrLn
                                   loggerset
                                   (logNS
                                        utctime
                                        INFO
                                        FBAction
                                        { _who = userId
                                              (fromJust (commentFrom c))
                                        , _where = postId p
                                        , _action = "comment"
                                        , _what = postId p
                                        , _entity = toJSON c
                                        }))
                         (filter
                              (\c ->
                                    fromMaybe utctime (commentCreatedTime c) >=
                                    utctime)
                              cs)
                     reposts <-
                         mapM
                             (\rp ->
                                   pushLogStrLn
                                       loggerset
                                       (logNS
                                            utctime
                                            INFO
                                            FBAction
                                            { _who = userId
                                                  (fromJust (postFrom rp))
                                            , _where = postId p
                                            , _action = "repost"
                                            , _what = postId p
                                            , _entity = toJSON rp
                                            }) >>
                                   return rp)
                             (filter
                                  (\rp ->
                                        fromMaybe utctime (postCreatedTime rp) >=
                                        utctime)
                                  rs)
                     return reposts)
            (filter
                 (\(p,_) ->
                       fromMaybe utctime (postCreatedTime p) >= utctime)
                 postsInfo)
    logAllPostsInfo loggerset (concat reposts) utctime env'


logAllInfoAboutPage :: LoggerSet -> Id -> UTCTime -> Env u -> IO ()
logAllInfoAboutPage loggerset pid utctime env' = do
    (page,users,posts) <-
        collectAllInfoAboutPage pid (shiftTo2weeksBack utctime) env'
    pushLogStrLn
        loggerset
        (logNS
             utctime
             INFO
             FBAction
             { _who = Id "someone"
             , _where = Id "facebook"
             , _action = "updated"
             , _what = pageId page
             , _entity = toJSON page
             })
    mapM_
        (\u ->
              pushLogStrLn
                  loggerset
                  (logNS
                       utctime
                       INFO
                       FBAction
                       { _who = userId u
                       , _where = Id "facebook"
                       , _action = "like"
                       , _what = pageId page
                       , _entity = toJSON u
                       }))
        users
    mapM_
        (\p ->
              pushLogStrLn
                  loggerset
                  (logNS
                       (fromMaybe utctime (postCreatedTime p))
                       INFO
                       FBAction
                       { _who = userId (fromJust (postFrom p))
                       , _where = pageId page
                       , _action = "post"
                       , _what = postId p
                       , _entity = toJSON p
                       }))
        (filter
             (\p ->
                   fromMaybe utctime (postCreatedTime p) >= utctime)
             posts)
    logAllPostsInfo loggerset posts utctime env'
