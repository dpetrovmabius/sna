#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import ujson
import logging
import time


class NSFormatter(logging.Formatter):
    def __init__(self):
        logging.Formatter.__init__(self)

    def format(self, record):
        return ujson.dumps({
            "time": int(time.time()),
            "severity": record.levelname,
            "logMessage": record.msg,
            "sourceLocation": {
                "file": record.filename,
                "line": record.lineno,
                "functionName": record.funcName
            }
        })
