#!/usr/bin/env python
#-*- coding: utf-8 -*-

import ujson
import asyncio
import aiohttp


async def vkapi(session, method, params=None, access_token=None, retry=8):
    """
    Request vk api method with given params

    Args:
      method :: str
      params :: dict
      access_token :: str
    Return:
      dict | VKError
    """
    baseurl = "https://api.vk.com/method/{}{}{}"
    if params is not None:
        params_url = "?{}".format(to_params(params))
    else:
        params_url = ""
    if access_token is not None:
        at_url = "&access_token={}".format(access_token)
    else:
        at_url = ""
    url = baseurl.format(method, params_url, at_url)
    try:
        with aiohttp.Timeout(10):
            response = await session.get(url)
    except Exception as e:
        print("Session get error ", repr(e))
        if retry < 256:
            await asyncio.sleep(retry)
            return ( await vkapi(session, method, params, access_token,
                                 retry << 1))
        else:
            raise e
    try:
        result = await response.json()
    except Exception as e:
        print(e)
        raise VKError(code="-1", msg="response.json error: {}".format(repr(e)))

    if "error" in result:
        raise VKError(code=result["error"]["error_code"],
                      msg=result["error"]["error_msg"])
    elif "response" in result:
        return result["response"]
    else:
        raise VKError(code=-1, msg="Unknown result: {}".format(result))


async def vkcomment(logger,
              bfqueue,
              session,
              app_id,
              entity_id,
              id_,
              timestamp,
              offset=0,
              count=100):
    """
    Grab all possible information about post' comments

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
    Return
      Bool
    """
    owner_id = id_.split('_')[0]
    post_id = id_.split('_')[1]
    try:
        params = {
            "owner_id": owner_id,
            "post_id": post_id,
            "need_likes": 1,
            "offset": offset,
            "count": count,
            "preview_length": 0
        }
        comments = await vkapi(session,
                               method="wall.getComments",
                               params=params)
    except VKError as e:
        logger.exception(repr(e))
        return False
    for comment in comments[1:]:
        if int(comment["date"]) > timestamp:
            logger.info({
                "params": params,
                "source": "vk",
                "app_id": app_id,
                "entity_id": entity_id,
                "method": "wall.getComments",
                "result":
                {"owner_id": owner_id,
                 "post_id": post_id,
                 "comment": comment}
            })

    if ((comments[0] > offset + count) and
        (int(comment["date"]) > timestamp - 14 * 24 * 60 * 60)):
        return ( await vkcomment(logger, bfqueue, session, app_id, entity_id,
                                 id_, timestamp, offset + count, count))


async def vklike(logger,
           bfqueue,
           session,
           app_id,
           entity_id,
           id_,
           timestamp,
           offset=0,
           count=100,
           type_="post"):
    """
    Grab all possible information younger than timestamp about post

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
      offset :: int
      count :: int
      type_ :: str  -- ^ https://vk.com/dev/likes.getList
    Return
      Bool
    """
    owner_id = id_.split('_')[0]
    type_id = id_.split('_')[1]
    try:
        params = {
            "type": type_,
            "owner_id": owner_id,
            "item_id": type_id,
            "offset": offset,
            "count": count
        }
        getList = await vkapi(session, method="likes.getList", params=params)
    except VKError as e:
        logger.exception(repr(e))
        return False

    for user in getList["users"]:
        logger.info({
            "params": params,
            "source": "vk",
            "app_id": app_id,
            "entity_id": entity_id,
            "method": "likes.getList",
            "result":
            {"owner_id": owner_id,
             "item_id": type_id,
             "user_id": user}
        })
        result = await vkuser(logger, bfqueue, session, app_id, entity_id,
                              user, timestamp)
    if getList["count"] > offset + count:
        return ( await (vklike(logger, bfqueue, session, app_id, entity_id,
                               id_, timestamp, offset + count, count, type_)))
    return True


async def vkrepost(logger,
             bfqueue,
             session,
             app_id,
             entity_id,
             id_,
             timestamp,
             offset=0,
             count=100):
    """
    Grab all possible information younger than timestamp about post

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
      offset :: int
      count :: int
    Return
      Bool
    """
    owner_id = id_.split('_')[0]
    post_id = id_.split('_')[1]

    try:
        params = params = {
            "owner_id": owner_id,
            "post_id": post_id,
            "offset": offset,
            "count": count
        }
        getReposts = await vkapi(session,
                                 method="wall.getReposts",
                                 params=params)
    except VKError as e:
        logger.exception(repr(e))
        return False
    for post in filter(
        lambda p: int(p["date"]) > timestamp - 14 * 24 * 60 * 60,
        getReposts["items"]):
        logger.info({
            "params": params,
            "source": "vk",
            "app_id": app_id,
            "entity_id": entity_id,
            "method": "wall.getReposts",
            "result": {
                "owner_id": owner_id,
                "post_id": post_id,
                "to_id": post["to_id"],
                "new_post_id": post["id"]
            }
        })
        result = await bfqueue.put(
            "vk_post_{}".format("{}_{}".format(post["to_id"], post["id"])),
            (app_id, entity_id, "vk", "post",
             "{}_{}".format(post["to_id"], post["id"]), timestamp))
    return False


async def vkpost(logger, bfqueue, session, app_id, entity_id, id_, timestamp):
    """
    Grab all possible information younger than timestamp about post

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
    Return
      Bool
    """
    try:
        params = {"posts": id_}
        getById = await vkapi(session, method="wall.getById", params=params)
    except VKError as e:
        logger.exception(repr(e))
        return False
    if not getById:
        logger.warning(repr({
            "source": "vk",
            "app_id": app_id,
            "entity_id": entity_id,
            "id": id_,
            "result": getById
        }))
        return False
    if int(getById[0]["date"]) > int(timestamp):
        logger.info({
            "params": params,
            "source": "vk",
            "app_id": app_id,
            "entity_id": entity_id,
            "method": "wall.getById",
            "result": getById
        })
    comments_result = await vkcomment(logger, bfqueue, session, app_id,
                                      entity_id, id_, timestamp)
    likes_result = await vklike(logger, bfqueue, session, app_id, entity_id,
                                id_, timestamp)
    reposts_result = await vkrepost(logger, bfqueue, session, app_id,
                                    entity_id, id_, timestamp)
    return comments_result and likes_result and reposts_result


async def vkfriends(logger, bfqueue, session, app_id, entity_id, id_, timestamp):
    """
    Grab user friends

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
    Return
      Bool
    """
    try:
        params = {"user_id": id_}
        friends = await vkapi(session, "freinds.get", params=params)
    except VKError as e:
        logger.exception(repr(e))
        return False
    if not friends:
        logger.warning(repr({
            "source": "vk",
            "app_id": app_id,
            "entity_id": entity_id,
            "id": id_,
            "result": friends
        }))
        return False
    for friend in friends:
        logger.info({
            "params": params,
            "source": "vk",
            "app_id": app_id,
            "entity_id": entity_id,
            "id": id_,
            "result": {
                "user_id": id_,
                "friend_id": friend,
                "action": "friends",
                "description": "{} and {} freinds".format(id_, friend)
            }
        })
        await fbqueue.put("vk_user_{}".format(friend),
                          (app_id, entity_id, "vk", "user", friend, timestamp))
    return True


async def vkuser(logger, bfqueue, session, app_id, entity_id, id_, timestamp):
    """
    Grab allposible information about user

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
    Return
      Bool
    """
    try:
        params = {
            "user_id": id_,
            "fields":
            ["nickname", "screen_name", "sex", "bdate", "city", "country",
             "timezone", "photo_400_orig", "has_mobile", "contacts",
             "education", "counters", "relation", "activity", "universities",
             "followers_count", "occupation", "personal", "exports"]
        }
        user = await vkapi(session, "users.get", params=params)
    except VKError as e:
        logger.exception(repr(e))
        return False
    if not user:
        logger.warning(repr({
            "source": "vk",
            "app_id": app_id,
            "entity_id": entity_id,
            "id": id_,
            "result": user
        }))
        return False
    logger.info({
        "params": params,
        "app_id": app_id,
        "entity_id": entity_id,
        "source": "vk",
        "method": "users.get",
        "result": user
    })
    # result = await vkwall(logger, bfqueue, session, app_id, entity_id,
    #                      user[0]["uid"], timestamp)
    return True


async def vkwall(logger,
           bfqueue,
           session,
           app_id,
           entity_id,
           id_,
           timestamp,
           offset=0,
           count=100):
    """
    Grab all possible information younger than timestamp about group

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
    Return:
      Bool
    """
    try:
        params = {"owner_id": id_, "extended": 1, "offset": offset, "count": count}
        wall = await vkapi(session, method="wall.get", params=params)
    except VKError as e:
        logger.exception(repr(e))
        return False
    post = False
    for post in filter(
        lambda p: int(p["date"]) > timestamp - 14 * 24 * 60 * 60, wall.get(
            "wall", [])[1:]):
        logger.info({
            "params": params,
            "app_id": app_id,
            "entity_id": entity_id,
            "method": "wall.get",
            "result": {"type": "post",
                       "id": post["id"],
                       "post": post}
        })
        comments_result = await vkcomment(logger, bfqueue, session, app_id,
                                          entity_id, "{}_{}".format(id_,post["id"]), timestamp)
        likes_result = await vklike(logger, bfqueue, session, app_id, entity_id,"{}_{}".format(id_,post["id"])
                            , timestamp)
        reposts_result = await vkrepost(logger, bfqueue, session, app_id,
                                    entity_id, "{}_{}".format(id_,post["id"]), timestamp)
 
    for user in wall.get("profiles", [])[1:]:
        logger.info({
            "params": params,
            "app_id": app_id,
            "entity_id": entity_id,
            "method": "wall.get",
            "result": {"type": "user",
                       "id": user["uid"],
                       "user": user}
        })
    if (int(wall.get("wall", [0])[0]) > offset + count):
        return ( await vkwall(logger, bfqueue, session, app_id, entity_id, id_,
                              timestamp, offset + count, count))
    return True


async def vkgroupmembers(logger,
            bfqueue,
            session,
            app_id,
            entity_id,
            id_,
            timestamp,
            offset=0,
                          count=1000):
    """
    Grab group members

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
    Return:
      Bool
    """
    try:
        params = {"group_id": id_, "offset": offset, "count": count}
        getMembers = await vkapi(session, method="groups.getMembers", params=params)
    except VKError as e:
        print(e)
        logger.exception(repr(e))
        return False
    for uid in getMembers.get("users", []):
        logger.info({
            "params": params,
            "app_id": app_id,
            "entity_id": entity_id,
            "method": "groups.getMembers",
            "result": {"type": "user",
                       "id": uid}
         })
        #result = await bfqueue.put(
        #    "vk_user_{}".format(uid),
        #    (app_id, entity_id, "vk", "user", uid, timestamp))
    print(int(getMembers.get("count", 0)), offset + count)
    if int(getMembers.get("count", 0)) >= offset + count:
        return (await vkgroupmembers(logger, bfqueue, session, app_id, entity_id, id_, timestamp, offset + count, count))
    return True


async def vkgroup(logger,
            bfqueue,
            session,
            app_id,
            entity_id,
            id_,
            timestamp,
            offset=0,
            count=100):
    """
    Grab all possible information younger than timestamp about group

    Args:
      logger :: logging.Logger
      app_id :: str
      entity_id :: str
      id_ :: str
      timestamp :: int
    Return:
      Bool
    """
    try:
        params = {
            "group_id": id_,
        }
        getById = await vkapi(session, method="groups.getById", params=params)
    except VKError as e:
        logger.exception(repr(e))
        return False
    if not getById:
        logger.warning(repr({
            "source": "vk",
            "app_id": app_id,
            "entity_id": entity_id,
            "id": id_,
            "result": getById
        }))
        return False
    if "gid" not in getById[0]:
        logger.exception(
            "There is no gid in response of vk api method groups.getById with " \
            "params group_id={}".format(id_))
        return False
    logger.info({
        "params": params,
        "app_id": app_id,
        "entity_id": entity_id,
        "method": "groups.getById",
        "result": getById
    })
    result = await vkgroupmembers(logger, bfqueue, session, app_id, entity_id,
                          "{}".format(getById[0]["gid"]), timestamp)
    result = await vkwall(logger, bfqueue, session, app_id, entity_id,
                          "-{}".format(getById[0]["gid"]), timestamp)
    return result


async def vk(logger, bfqueue, session, app_id, entity_id, type_, id_, timestamp):
    if type_ == "group":
        return ( await vkgroup(logger, bfqueue, session, app_id, entity_id,
                               id_, timestamp))
    elif type_ == "post":
        return ( await vkpost(logger, bfqueue, session, app_id, entity_id, id_,
                              timestamp))
    elif type_ == "user":
        return ( await vkuser(logger, bfqueue, session, app_id, entity_id, id_,
                              timestamp))


# --- Utils ---
def to_params(params):
    """
    Stringify dict with params to url query params

    Args:
      params :: dict
    Return:
      str
    """

    def stringify(value):
        if isinstance(value, list):
            return ','.join(value)
        else:
            return str(value)

    return '&'.join(["{}={}".format(k, stringify(v)) for k, v in params.items()
                     ])


class VKError(Exception):
    def __init__(self, code, msg):
        self.code = code
        self.message = msg

    def __str__(self):
        return "VKError: {} {}".format(self.code, self.message)

    def __repr__(self):
        return ujson.dumps(
            {"code": self.code,
             "message": self.message,
             "error": "VKError"})
