#!/usr/bin/env python
#-*- coding: utf-8 -*-

import asyncio


class BFQueue(object):
    """
    BFQueue is asyncio.Queue with Bloom Filter. We add task only when there is no in queue
    """
    def __init__(self, capacity, error_rate, maxsize=0, loop=None):
        """
        Initialize BFQueue

        Args:
          capacity :: int  -- ^ Bloom Filter capacity parameter
          error_rate :: double  -- ^ Bloom Filter error_rate parameter
          maxsize :: int  -- ^ asyncio.Queue maxsize parameter
          loop :: EventLoop
        Returns:
         Raises:
        """
        self.__q = asyncio.PriorityQueue(maxsize=maxsize, loop=loop)
        self.__bf = set() # pybloom.BloomFilter(capacity=capacity,
                          #              error_rate=error_rate, filename=None)


    async def put(self, item_id, item):
        """
        Put item to queue if item_id not in BloomFilter

        Args:
          item_id :: str
          item :: object
        Returns:
          bool
        Raises:
        """
        if item_id not in self.__bf:
            priority = 0
            if item[3] == "group":
                priority = 2
            elif item[3] == "post":
                priority = 1
            elif item[3] == "user":
                priority = 0
            print("Put ", priority, item)
            self.__bf.add(item_id)
            result = await self.__q.put((priority, item))
        return True

    async def get(self):
        """
        Return item from queue

        Args:
        Retuns:
          queue element
        Raises:
        """
        result = await self.__q.get()
        print("Start evaluate ", result)
        return result[1]

    async def join(self):
        return (await self.__q.join())

    def empty(self):
        return self.__q.empty()

    def qsize(self):
        return self.__q.qsize()

    def maxsize(self):
        return self.__q.maxsize
