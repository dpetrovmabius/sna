#!/usr/bin/env python
#-*- coding: utf-8 -*-

import time
import asyncio
import aiohttp
import sqlite3
import logging
import logging.handlers
from optparse import OptionParser
from .nsformatter import NSFormatter
from .vkontakte import vk
from .facebook import fb
from .bfqueue import BFQueue


async def grab(logger, bfqueue, session, app_id, entity_id, source, type_, id_,
         timestamp):
    """
    Grab info about entity_id from source and save in logs

    Args:
      logger :: logging.Logger
      bfqueue :: bfqueue.BFQueue
      session :: aiohttp.ClientSession
      app_id :: str
      entity_id :: str
      source :: facebook | vk
      type_ :: post | group | user
      id_ :: str
      timestamp :: int
    Return:
      Bool
    """
    if source == "vk":
        result = await vk(logger, bfqueue, session, app_id, entity_id, type_,
                          id_, timestamp)
    elif source == "facebook":
        result = await fb(logger, bfqueue, session, app_id, entity_id, type_,
                          id_, timestamp)
    return result


async def consumer(logger, bfqueue, slept=False):
    session = aiohttp.ClientSession()
    while not bfqueue.empty():
        task = await bfqueue.get()
        result = await grab(logger, bfqueue, session, *task)
    session.close()
    if not slept:
        print("Consumer go to sleep")
        await asyncio.sleep(60)
        return ( await consumer(logger, bfqueue, True))
    return True


async def producer(logger, cursor, db, bfqueue):
    """
    Read tasks from sqlite db and if task
    lives longer than now create
    functions for processing them and after
    update last_modified

    Args:
      logger :: logging.Logger
      cursor :: sqlite3.cursor
      db :: sqlite3.connection
    Return:
      None
    """
    cursor.execute("SELECT * FROM register WHERE last_modified <= ?",
                   (int(time.time()), ))
    for task in cursor.fetchall():
        result = await bfqueue.put(
            "{}_{}_{}".format(task[2], task[3], task[4]), task)
        cursor.execute(
                "UPDATE register SET last_modified = ? WHERE " \
                "(app_id = ? AND entity_id = ? AND source = ? AND type = ? AND id = ?)",
                (int(time.time()), *task[:-1]))
        db.commit()


def main():
    import sys

    # We need go deeper :)
    sys.setrecursionlimit(100000)

    parser = OptionParser()
    parser.add_option("--db", dest="path_to_db", help="Path to sqlite db file")
    (options, args) = parser.parse_args()

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    ch = logging.handlers.RotatingFileHandler("logs/crawler.log",
                                     encoding="utf-8",
                                     maxBytes=1024 * 1024 * 1024)
    ch.setLevel(logging.INFO)
    formatter = NSFormatter()
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    db = sqlite3.connect(options.path_to_db)
    cursor = db.cursor()

    loop = asyncio.get_event_loop()

    bfqueue = BFQueue(capacity=100000, error_rate=0.001, maxsize=0)

    tasks = [asyncio.ensure_future(producer(logger, cursor, db, bfqueue))
            ,asyncio.ensure_future( consumer(logger, bfqueue))]
    try:
        loop.run_until_complete(asyncio.wait(tasks))
    except KeyboardInterrupt:
        pass
    db.close()
    loop.close()
