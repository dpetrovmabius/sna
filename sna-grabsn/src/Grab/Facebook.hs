{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Grab.Facebook
    ( grabPage
    ) where

import Data.Functor ((<$>))
import Data.Conduit (sourceToList)
import Data.Aeson (toJSON, ToJSON, FromJSON)
import Data.Monoid ((<>))
import Data.Text (Text)
import Data.Time (UTCTime)
import Data.Maybe (fromMaybe, fromJust)
import System.Log.FastLogger (LoggerSet, pushLogStrLn)
import Control.Monad.Trans.Resource
       (runResourceT, ResourceT)
import qualified Facebook as F
import qualified Network.HTTP.Conduit as H

import NSLogger

import Grab.Facebook.FBUtils


grabPage :: LoggerSet
         -> F.Credentials
         -> F.AppAccessToken
         -> H.Manager
         -> Text
         -> UTCTime
         -> IO ()
grabPage loggerset creds tok manager id_ utctime = do
    page <-
        runResourceT $
        F.runFacebookT
            creds
            manager
            (F.getPage
                 (F.Id id_)
                 [ ( "fields"
                   , "id,likes,name,talking_about_count,phone,checkins," <>
                     "category,can_post,website,is_published")]
                 (Just tok))
    pushLogStrLn
        loggerset
        (logNS
             utctime
             INFO
             FBAction
             { _who = F.Id "someone"
             , _where = F.Id "facebook"
             , _action = "updated"
             , _what = F.pageId page
             , _entity = toJSON page
             })
    grabPagePosts loggerset creds tok manager (F.pageId page) utctime

grabPagePosts :: LoggerSet
         -> F.Credentials
         -> F.AppAccessToken
         -> H.Manager
         -> F.Id
         -> UTCTime
         -> IO ()
grabPagePosts loggerset creds tok manager pageid utctime = do
    posts <-
        pagerA2listA
            creds
            manager
            (F.getPagePosts
                 pageid
                 [ ("fields", "id,shares,from,type,created_time,message")
                 , ("since", utc2bytestring (shiftTo2weeksBack utctime))]
                 (Just tok))
    mapM
        (\p ->
              pushLogStrLn
                  loggerset
                  (logNS
                       (fromMaybe
                            utctime
                            (F.unFbUTCTime <$> F.postCreatedTime p))
                       INFO
                       FBAction
                       { _who = F.userId (fromJust (F.postFrom p))
                       , _where = pageid
                       , _action = "post"
                       , _what = F.postId p
                       , _entity = toJSON p
                       }) >>
              return (F.postId p))
        (filter
             (\p ->
                   fromMaybe utctime (F.unFbUTCTime <$> F.postCreatedTime p) >=
                   utctime)
             posts) >>=
        grabPosts loggerset creds tok manager utctime pageid


grabPosts :: LoggerSet
          -> F.Credentials
          -> F.AppAccessToken
          -> H.Manager
          -> UTCTime
          -> F.Id
          -> [F.Id]
          -> IO ()
grabPosts _ _ _ _ _ _ [] = return ()
grabPosts loggerset creds tok manager utctime pageid postsid =
    grabPostsLikes loggerset creds tok manager utctime pageid postsid >>
    grabPostsComments loggerset creds tok manager utctime pageid postsid >>
    grabPostsReposts loggerset creds tok manager utctime pageid postsid


grabPostsLikes :: LoggerSet
               -> F.Credentials
               -> F.AppAccessToken
               -> H.Manager
               -> UTCTime
               -> F.Id
               -> [F.Id]
               -> IO ()
grabPostsLikes loggerset creds tok manager utctime parentid =
    mapM_
        (\postid ->
              pagerA2listA creds manager (F.getLikes postid [("fields", "id")] tok) >>=
              mapM_
                  (\liker ->
                        pushLogStrLn
                            loggerset
                            (logNS
                                 utctime
                                 INFO
                                 FBAction
                                 { _who = F.userId liker
                                 , _where = parentid
                                 , _action = "like"
                                 , _what = postid
                                 , _entity = toJSON liker
                                 })))

grabPostsComments :: LoggerSet
                  -> F.Credentials
                  -> F.AppAccessToken
                  -> H.Manager
                  -> UTCTime
                  -> F.Id
                  -> [F.Id]
                  -> IO ()
grabPostsComments loggerset creds tok manager utctime parentid =
    mapM_
        (\postid ->
              pagerA2listA
                  creds
                  manager
                  (F.getComments
                       postid
                       [("fields", "id,from,created_time,like_count,message")]
                       tok) >>=
              \comments ->
                   mapM_
                       (\comment ->
                             pushLogStrLn
                                 loggerset
                                 (logNS
                                      (fromMaybe
                                           utctime
                                           (F.unFbUTCTime <$>
                                            F.commentCreatedTime comment))
                                      INFO
                                      FBAction
                                      { _who = fromMaybe
                                            (F.Id "someone")
                                            (F.userId <$> F.commentFrom comment)
                                      , _where = parentid
                                      , _action = "comment"
                                      , _what = postid
                                      , _entity = toJSON comment
                                      }))
                       (filter
                            (\comment ->
                                  fromMaybe
                                      utctime
                                      (F.unFbUTCTime <$>
                                       F.commentCreatedTime comment) >=
                                  utctime)
                            comments))


grabPostsReposts
    :: LoggerSet
    -> F.Credentials
    -> F.AppAccessToken
    -> H.Manager
    -> UTCTime
    -> F.Id
    -> [F.Id]
    -> IO ()
grabPostsReposts loggerset creds tok manager utctime parentid =
    mapM_
        (\postid ->
              pagerA2listA
                  creds
                  manager
                  (F.getSharedPosts
                       postid
                       [("fields", "id,from,shares,created_time,type")]
                       tok) >>=
              \reposts ->
                   mapM_
                       (\repost ->
                             pushLogStrLn
                                 loggerset
                                 (logNS
                                      utctime
                                      INFO
                                      FBAction
                                      { _who = fromMaybe
                                            (F.Id "someone")
                                            (F.userId <$> F.postFrom repost)
                                      , _where = parentid
                                      , _action = "repost"
                                      , _what = postid
                                      , _entity = toJSON repost
                                      }) >>
                             grabPosts
                                 loggerset
                                 creds
                                 tok
                                 manager
                                 utctime
                                 (fromMaybe
                                      (F.Id "someone")
                                      (F.userId <$> F.postFrom repost))
                                 [F.postId repost])
                       (filter
                            (\repost ->
                                  fromMaybe
                                      utctime
                                      (F.unFbUTCTime <$>
                                       F.postCreatedTime repost) >=
                                  utctime)
                            reposts))


pagerA2listA
    :: (ToJSON a, FromJSON a)
    => F.Credentials
    -> H.Manager
    -> F.FacebookT F.Auth (ResourceT IO) (F.Pager a)
    -> IO [a]
pagerA2listA creds manager getA = do
    pagerA <- runResourceT $ F.runFacebookT creds manager getA
    sourceA <-
        runResourceT $
        F.runFacebookT creds manager (F.fetchAllNextPages pagerA)
    sourceToList sourceA
