{-# LANGUAGE OverloadedStrings #-}
module Grab.Facebook.FBUtils
    ( FBAction(..)
    , utc2bytestring
    , shiftTo2weeksBack
    ) where

import Data.Aeson (ToJSON, Value, object, toJSON, (.=))
import qualified Data.ByteString.Char8 as BSC
import Data.Time
       (UTCTime, formatTime, defaultTimeLocale, addUTCTime)
import Facebook (Id(..))
import Data.Text (Text)


-- | Convert utc time to unix timestamp and convert to bytestirng
utc2bytestring :: UTCTime -> BSC.ByteString
utc2bytestring = BSC.pack . formatTime defaultTimeLocale "%s"

shiftTo2weeksBack :: UTCTime -> UTCTime
shiftTo2weeksBack = addUTCTime (-14 * 24 * 60 * 60)


data FBAction = FBAction
    { _who :: Id
    , _where :: Id
    , _what :: Id
    , _action :: Text
    , _entity :: Value
    }

instance ToJSON FBAction where
    toJSON (FBAction _who _where _what _action _entity) =
        object
            [ "who" .= idCode _who
            , "where" .= idCode _where
            , "action" .= _action
            , "what" .= idCode _what
            , "entity" .= _entity]
