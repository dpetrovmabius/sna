
module Grab
    ( grabFbPage
    ) where

import Data.Text (Text)
import Data.Time (UTCTime)
import System.Log.FastLogger (LoggerSet)
import Facebook (Credentials, AppAccessToken)
import Network.HTTP.Conduit (Manager)
import qualified Grab.Facebook as Fb


grabFbPage :: LoggerSet
         -> Credentials
         -> AppAccessToken
         -> Manager
         -> Text
         -> UTCTime
         -> IO ()
grabFbPage = Fb.grabPage
