{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module NSLog where

import Data.Time (formatTime, defaultTimeLocale, UTCTime)
import GHC.IO.Handle (Handle)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Catch (MonadMask)
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy.Char8 as BL
import Data.Aeson (ToJSON(..), encode, (.=), object, toJSON, Value)
import Control.Monad.Log
       (WithSeverity(..), WithTimestamp(..), MonadLog, BatchingOptions,
        Handler, withBatchedHandler, Severity(..), timestamp, logMessage)


-- | NewSquare log format
newtype WithNSFormat a =
    WithNSFormat (WithSeverity (WithTimestamp a))

instance ToJSON Severity where
    toJSON Error = "ERROR"
    toJSON Informational = "INFO"
    toJSON Emergency = "EMERGENCY"
    toJSON Alert = "ALERT"
    toJSON Critical = "CRITICAL"
    toJSON Warning = "WARNING"
    toJSON Notice = "NOTICE"
    toJSON Debug = "DEBUG"

instance ToJSON a => ToJSON (WithNSFormat a) where
    toJSON (WithNSFormat (WithSeverity s (WithTimestamp a u))) =
        object
            [ "time" .= toJSON (formatTime defaultTimeLocale "%s" u)
            , "severity" .= toJSON s
            , "logMessage" .= toJSON a]



-- | Log in NewSquare format. Automaticaly set timestamp
logWithNSFormat
    :: (ToJSON a, MonadLog (WithNSFormat a) m, MonadIO m)
    => Severity -> a -> m ()
logWithNSFormat severity message =
    timestamp message >>= logMessage . WithNSFormat . WithSeverity severity


-- | If you want set custom timestamp use this function
logWithTimesampNSFormat
    :: (ToJSON a, MonadLog (WithNSFormat a) m)
    => Severity -> UTCTime -> a -> m ()
logWithTimesampNSFormat severity utctime message =
    logMessage $
    WithNSFormat $ WithSeverity severity $ WithTimestamp message utctime



-- | Output logs in JSON format
withJSONHandler
    :: (ToJSON message, MonadIO io, MonadMask io)
    => BatchingOptions -> Handle -> (Handler io message -> io a) -> io a
withJSONHandler options fd =
    withBatchedHandler options (mapM_ (BL.hPutStrLn fd . encode))
