{-# LANGUAGE DeriveGeneric #-}
module SNA.Types
    ( SNAResponse(..)
    , AppM
    , SNAState(..)
    , SNAAction(..)
    , Action(..)
    ) where

import Data.Text (Text)
import GHC.Generics (Generic)
import Data.Aeson (ToJSON(..), Value)
import Control.Monad.Trans.Reader (ReaderT)
import Control.Monad.Trans.Either (EitherT)
import Database.SQLite.Simple (Connection)
import Servant.Server (ServantErr)
import System.Log.FastLogger (LoggerSet)


data SNAState = SNAState
    { connection :: Connection
    , loggerset :: LoggerSet
    }

type AppM = EitherT ServantErr (ReaderT SNAState IO)

newtype SNAResponse a = SNAResponse a
    deriving (Eq,Ord,Show,Generic)

instance ToJSON a => ToJSON (SNAResponse a)


data SNAAction = SNAAction
    { _who :: Text
    , _what :: Text
    , _action :: Action
    , _entity :: Value
    } deriving (Eq,Show,Generic)

instance ToJSON SNAAction


data Action
    = Insert
    | Delete
    | Error Action
    deriving (Eq,Ord,Show,Generic)

instance ToJSON Action
