{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RecordWildCards #-}
module SNA.Facebook
    ( FacebookAPI
    , fbServer
    ) where

import Control.Applicative()
import Data.Time (UTCTime, getCurrentTime, addUTCTime)
import Data.Text (Text)
import Data.Text.Lazy (fromStrict)
import Data.Text.Lazy.Encoding (encodeUtf8)
import GHC.Generics (Generic)
import Data.Aeson (FromJSON, ToJSON(..))
import Control.Monad.Trans.Reader (ask)
import Database.SQLite.Simple
       (execute, ToRow(..), FromRow(..), field, Only(..), Connection,
        Query, SQLError(..), Error(..))
import Database.SQLite.Simple.ToField (toField)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Either (left, right)
import Control.Monad.Trans.Class (lift)
import Control.Exception (try)
import qualified Data.ByteString.Lazy as LBS
import System.Log.FastLogger (pushLogStrLn)
import Servant
import NSLogger
import SNA.Types


type FacebookAPI = "facebook" :> CombinedFbAPI
type CombinedFbAPI = "pages" :> FBPageAPI :<|> "users" :> FBUserAPI


type FBPageAPI = ReqBody '[JSON] Page :> Post '[JSON] (SNAResponse ())  -- add a page
            :<|> Capture "pageid" Text :> Delete '[JSON] (SNAResponse ())  -- delete page


data Page = Page
    { pageId :: Text
    } deriving (Eq,Show,Ord,Generic)


data FBPage = FBPage
    { fbPageId :: Text
    , fbPageCreatedTime :: UTCTime
    , fbPageUpdatedTime :: UTCTime
    } deriving (Eq,Show,Ord,Generic)


instance FromJSON Page
instance ToJSON Page

instance ToRow FBPage where
    toRow FBPage{..} =
        [toField fbPageId, toField fbPageCreatedTime, toField fbPageUpdatedTime]


instance FromRow FBPage where
    fromRow = FBPage <$> field <*> field <*> field


toFbPage :: Page -> IO FBPage
toFbPage p =
    getCurrentTime >>=
    \utctime ->
         return (FBPage (pageId p) utctime utctime)


pageServer :: ServerT FBPageAPI AppM
pageServer = newPage :<|> deletePage
  where
    newPage :: Page -> AppM (SNAResponse ())
    newPage page = do
        state <- lift ask
        fbpage <- liftIO $ toFbPage page
        liftIO $
            logNSINFO
                SNAAction
                { _who = "me"
                , _what = pageId page
                , _action = Insert
                , _entity = toJSON page
                } >>=
            pushLogStrLn (loggerset state)
        ans <-
            liftIO $
            dbexecute
                (connection state)
                "INSERT INTO fbPage (pid,created_time,updated_time) VALUES (?,?,?)"
                fbpage
        case ans of
            Left err ->
                liftIO
                    (logNSINFO
                         SNAAction
                         { _who = "me"
                         , _what = pageId page
                         , _action = Error Insert
                         , _entity = toJSON page
                         } >>=
                     pushLogStrLn (loggerset state)) >>
                returnErr err
            Right _ -> right (SNAResponse ())
    deletePage :: Text -> AppM (SNAResponse ())
    deletePage pid = do
        state <- lift ask
        liftIO $
            logNSINFO
                SNAAction
                { _who = "me"
                , _what = pid
                , _action = Insert
                , _entity = toJSON pid
                } >>=
            pushLogStrLn (loggerset state)
        ans <-
            liftIO $
            dbexecute
                (connection state)
                "DELETE FROM fbPage WHERE pid = ?"
                (Only pid)
        case ans of
            Left err ->
                liftIO
                    (logNSINFO
                         SNAAction
                         { _who = "me"
                         , _what = pid
                         , _action = Error Insert
                         , _entity = toJSON pid
                         } >>=
                     pushLogStrLn (loggerset state)) >>
                returnErr err
            Right _ -> right (SNAResponse ())



type FBUserAPI = ReqBody '[JSON] User :> Post '[JSON] (SNAResponse ())  -- add a user
            :<|> Capture "userid" Text :> Delete '[JSON] (SNAResponse ())  -- delete user


data User = User
    { userId :: Text
    , userToken :: Text
    } deriving (Eq,Ord,Show,Generic)

data FBUser = FBUser
    { fbUserId :: Text
    , fbUserToken :: Text
    , fbUserCreatedTime :: UTCTime
    , fbUserUpdatedTime :: UTCTime
    } deriving (Eq,Show,Ord,Generic)

instance FromJSON User
instance ToJSON User

instance FromRow FBUser where
  fromRow = FBUser <$> field <*> field <*> field <*> field

instance ToRow FBUser where
    toRow FBUser{..} =
        [ toField fbUserId
        , toField fbUserToken
        , toField fbUserCreatedTime
        , toField fbUserUpdatedTime]

toFbUser :: User -> IO FBUser
toFbUser user =
    getCurrentTime >>=
    \utctume ->
         return (FBUser (userId user) (userToken user) utctume utctume)

userServer :: ServerT FBUserAPI AppM
userServer = newUser :<|> deleteUser
  where
    newUser :: User -> AppM (SNAResponse ())
    newUser user = do
        state <- lift ask
        fbuser <- liftIO $ toFbUser user
        ans <-
            liftIO $
            dbexecute
                (connection state)
                "INSERT INTO fbUser (uid,token,created_time,updated_time) VALUES (?,?,?,?)"
                fbuser
        case ans of
            Left err -> returnErr err
            Right _ -> right (SNAResponse ())
    deleteUser :: Text -> AppM (SNAResponse ())
    deleteUser uid = do
        state <- lift ask
        ans <-
            liftIO $
            dbexecute
                (connection state)
                "DELETE FROM fbUser WHERE uid = ?"
                (Only uid)
        case ans of
            Left err -> returnErr err
            Right _ -> right (SNAResponse ())


fbServer :: ServerT FacebookAPI AppM
fbServer = pageServer :<|> userServer


dbexecute
    :: ToRow a
    => Connection -> Query -> a -> IO (Either SQLError ())
dbexecute conn query a = try $ execute conn query a


returnErr :: SQLError -> AppM (SNAResponse a)
returnErr (SQLError ErrorConstraint detail _context) =
    left (modifyErrBody (encodeUtf8 (fromStrict detail)) err409)
returnErr (SQLError _ detail _context) =
    left (modifyErrBody (encodeUtf8 (fromStrict detail)) err500)


modifyErrBody :: LBS.ByteString -> ServantErr -> ServantErr
modifyErrBody newErrBody (ServantErr code phrase _body headers) =
    ServantErr code phrase newErrBody headers


shiftToMonthAgo :: UTCTime -> UTCTime
shiftToMonthAgo = addUTCTime (-30 * 24 * 60 * 60)
