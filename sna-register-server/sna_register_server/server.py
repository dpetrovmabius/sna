#!/usr/bin/env python
#-*- coding: utf-8 -*-

import time
import asyncio
from aiohttp import web

import logging


logging.basicConfig()
logging.getLogger("aiohttp.web").setLevel(logging.DEBUG)

import sqlite3


DB = sqlite3.connect("sna_register.db")
CURSOR = DB.cursor()
CURSOR.execute(
    """
    CREATE TABLE IF NOT EXISTS register
    (app_id,entity_id,source,type,id,last_modified)
    """)

SOURCES = set(["facebook", "vk"])
TYPES = set(["group", "post", "user"])


def registered(app_id, entity_id, source, type_, id_):

    CURSOR.execute(
        "SELECT (EXISTS(SELECT id FROM register " \
        "WHERE (app_id=? AND entity_id=? AND source=? AND type=? AND id=?)))",
        (app_id, entity_id, source, type_, id_))
    try:
        if CURSOR.fetchone()[0]:
            CURSOR.execute(
                "UPDATE register SET last_modified = ? " \
                "WHERE (app_id=? AND entity_id=? AND source=? AND type=? AND id=?)",
                (int(time.time()), app_id, entity_id, source, type_, id_))
        else:
            CURSOR.execute("INSERT INTO register VALUES (?,?,?,?,?,?)",
                           (app_id, entity_id, source, type_, id_,
                            int(time.time()) - 165 * 24 * 60 * 60))
        DB.commit()
    except sqlite3.IntegrityError:
        return False
    return True


async def register(request):
    data = await request.json()
    if ("app_id" not in data or "entity_id" not in data or
        "source" not in data or "type" not in data or "id" not in data):
        raise web.HTTPBadRequest(text="JSON is not correct")
    if data["source"] not in SOURCES:
        raise web.HTTPBadRequest(
            text="Source must be {}".format(" or ".join(list(SOURCES))))
    if data["type"] not in TYPES:
        raise web.HTTPBadRequest(
            text="type must be {}".format(" or ".join(list(TYPES))))

    if registered(data["app_id"], data["entity_id"], data["source"],
                  data["type"], data["id"]):
        return web.HTTPCreated(text="Registered")
    else:
        raise web.HTTPInternalServerError()


def main():
    app = web.Application()
    app.router.add_route("POST", "/register", register)

    loop = asyncio.get_event_loop()
    handler = app.make_handler()
    f = loop.create_server(handler, "0.0.0.0", 8080)
    srv = loop.run_until_complete(f)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print("KeyboardInterrupt: close server")
    finally:
        srv.close()
        loop.run_until_complete(srv.wait_closed())
        loop.run_until_complete(handler.finish_connections(1.0))
        loop.run_until_complete(app.finish())
    DB.close()
    loop.close()
